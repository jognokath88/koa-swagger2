# Working output

[![](./media/koa-swagger.png)](https://www.youtube.com/watch?v=cuw0cMjwqro "Working Output - koa swagger project by kate")


<iframe width="560" height="315" src="https://www.youtube.com/embed/cuw0cMjwqro" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<figure class="video_container">
  <iframe src="./media/koa-swagger.mkv" frameborder="0" allowfullscreen="true"> 
</iframe>
</figure>


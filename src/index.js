import Koa  from 'koa';
import swagger2Koa from 'swagger2-koa';
import * as swagger from 'swagger2';
import Router from '@koa/router';
import koaBody from 'koa-body';
import logger from 'koa-logger';

import { routes as registerRoute } from './routes/register.js';
import { routes as loginRoute } from './routes/login.js';
import { routes as postRoute } from './routes/posts.js';
import { koaSwagger } from 'koa2-swagger-ui';

const  validate  = swagger2Koa.validate;  // dynamic import for swagger2koa not working so validate was imported like this

const spec = swagger.loadDocumentSync('./src/swagger.yaml');
// console.log(ui,validate)
if (!swagger.validateDocument(spec)) {
    throw Error ('Invalid Swagger file');
}

const port = process.env.PORT || 3000;

const app = new Koa();

const router = Router({prefix: '/v1'});

for (const routes of [registerRoute, loginRoute, postRoute]) {
    routes(router);
}

app 
    .use(logger())
    .use(koaBody.koaBody())
    .use(validate(spec))
    .use(router.routes())
    .use(router.allowedMethods())
    .use( koaSwagger({
        routePrefix: '/docs', 
        swaggerOptions: { spec },
      })
    )
    .listen(port, () => console.log(`Listening at port ${port}`))